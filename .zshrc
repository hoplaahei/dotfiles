alias zr='source $HOME/.zshrc'
alias zeb='$EDITOR $HOME/.oh-my-zsh/custom/bash-compatible.zsh'
alias zer='$EDITOR $HOME/.oh-my-zsh/custom/zsh-specific.zsh'

alias cdl="cd !$ "

alias E="SUDO_EDITOR=\"emacsclient -t -a emacs\" sudoedit"

### zsh global aliases
alias -g G='| grep -i'

### hardware fixes

# rescan to detect hotplugged external esata drives
alias rs='echo 0 0 0 | sudo tee /sys/class/scsi_host/host*/scan'

### useful functions

# safely convert a symlink to an actual copy of the linked file
removelink() {
  [ -L "$1" ] && cp --remove-destination "$(readlink "$1")" "$1"
}

# find text in files
ft() {  grep -rnw $1 -e $2; }

### tricks
nt() {  parallel -j1 -N0 $1 ::: {1..1000}; }

### OS-specific

# archlinux specific

alias pi='sudo pacman -S'
alias pss='sudo pacman -Ss'
alias pq='sudo pacman -Ql'
alias pr='sudo pacman -Rcns'
alias y='yaourt -S --noconfirm'
alias Y='yaourt -S'
alias ys='yaourt -Ss'
alias rd='sudo pacman -Rcns $(pacman -Qtdq)' # clear deps after failed AUR build

# gentoo
alias ei='sudo emerge'
alias eu='sudo CONFIG_PROTECT="-*" emerge --depclean'
alias es='eix'
alias ed='eix -S'
alias ev='emerge -pv'
alias up='cd /usr/local/portage/app-misc/'

# gentoo configs
alias pk='$EDITOR /etc/portage/package.accept_keywords'
alias pu='$EDITOR /etc/portage/package.use'
alias pm='$EDITOR /etc/portage/package.unmask'

### systemd

alias SR='systemctl --user restart'
alias SS='systemctl --user start'
alias ST='systemctl --user stop'
alias SD='systemctl --user daemon-reload'
alias HI='sudo systemctl hibernate'
alias SU='sudo systemctl suspend'

alias e="$EDITOR"
alias s='sudo'
#alias ss='sudo $(history -p !!)' # doesn't work with zsh
#alias cdl='cd $(history -p !$)' # doesn't work with zsh
alias cx='chmod +x'
alias ka='killall'
alias xi='$EDITOR $HOME/.xinitrc'
alias xr='$EDITOR $HOME/.Xresources'
alias sx='startx'
alias fr='find / | grep -i'

### video
alias mp='mpv -vo=vaapi --hwdec=auto --autosub-match=fuzzy'
alias sub='filebot -get-subtitles --lang en'

### gaming
alias dx="game-mode $HOME/.bin/dxmp"
alias ui="$EDITOR $HOME/.wine/drive_c/GOG\ Games/Deus\ Ex\ GOTY/System/User.ini"
