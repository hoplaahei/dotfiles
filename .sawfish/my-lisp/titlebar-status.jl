(require 'rep.io.timers)
(require 'rep.util.utf8)

(define titlebar-status-timer
  (make-timer (lambda ()
                (titlebar-status-update)) 1))

(defun titlebar-status-update ()
  "Update the titlebar status"
  (set-timer titlebar-status-timer 1)
  (map-windows (lambda (x)
                 (let ((status-string (concat "[" (current-time-string nil "%d\/%m %H:%M") "] ")))
                   (if (string-match "^[[]...........[]] " (window-name x))
                       (let ((title-without-status (utf8-substring (window-name x) 14)))
                         (rename-window x (concat status-string title-without-status)))
                     (rename-window x (concat status-string (window-name x))))
                     ))))
