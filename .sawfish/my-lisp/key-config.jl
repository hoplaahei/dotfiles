;; *************************************** ;;
;; ************ Default Apps ************* ;;
;; *************************************** ;;
;; *************************************** ;;

;; The idea of this key setup is that each app is launched once 
;; with a 'Win + Letter' shortcut. The same shortcut then switches
;; between multiple instances of that app. e.g., switches between 
;; Firefox browser windows. To force a new instance when one is 
;; already running, hold 'Win + Ctrl' before pressing the 
;; corresponding letter. 

;; First choose your default apps below.

;; If you do not know the right class name for your apps
;; (find it with 'xprop' util), you can try to find running 
;; windows by name instead by toggling the class field to 'nil'.

;;    Application        Command                   Class?   Class/Name
(define apps
      '( ("fileman"          "spacefm"                    t     "Spacefm"  )
         ("browser"          "chromium"                    t     "Chromium"  )
         ("pdf"              "mupdf"                       t     "mupdf"     )
         ("editor"           "emacsclient -c"		  t   	"Emacs"	   )
         ("terminal"         "urxvtc"                     t     "URxvt"    )
         ("video"            "smplayer"                        t     "smplayer"      )
         ("paint"            "gimp"                       t     "Gimp"     )
         ("audio"            "urxvtc -e ncmpcpp"          nil   "ncmpcpp"  )
         ("mixer"            "pavucontrol"        t   "pavucontrol")
         ("todo"             "$HOME/.bin/editor todo.org" nil   "todo.org" )
         ("sawfish-config"   "sawfish-config"             t     "Sawfish-Configurator")
         ("proc-viewer"      "urxvtc -e glances -z"       nil   "glances")))


;; ********************************************************************** ;;
;; ************ Code To Generate Keybinds From Default Apps ************* ;;
;; ********************************************************************** ;;
;; ********************************************************************** ;;

(require 'app)
(require 'sawfish.wm.commands.x-cycle)

(defun make-cycle-bind (app command useClass match)
  "Generate keybinds for cycling apps"
  (setq app-forwards (intern (format nil "cycle-%s" app))
        app-backwards (intern (format nil "cycle-%s-backwards" app)))

  (define-cycle-command-pair
    app-forwards app-backwards
    (app-selector (if (eq useClass t) (filter-byclass match)
                    (filter-byname match))
                    command)
    )
  (set (intern (eval app)) (eval command))
  )

;; Generate keybinds from apps listed at the start of this file
(mapcar #'(lambda (item)
                             ;; app        command    use class?     match
             (make-cycle-bind (car item) (cadr item) (caddr item) (cadddr item))
             )
         apps)

;; Now you can set your shorcuts like you would in a vanilla sawfish config

;; *************************************** ;;
;; *********** General Keybinds ********** ;;
;; *************************************** ;;
;; *************************************** ;;

( bind-keys global-keymap
"Super-C-Q" 'delete-group)

( bind-keys root-window-keymap
"Button3-Off" 'next-workspace-window)

( bind-keys title-keymap
"Button3-Click" 'next-workspace-window
"Button2-Click" 'cycle-group
)

( bind-keys iconify-button-keymap
"Button2-Click" 'maximize-fill-window-vertically-toggle
"Button3-Click" 'maximize-fill-window-horizontally-toggle)

;; *************************************** ;;
;; ************ App shortcuts ************ ;;
;; *************************************** ;;
;; *************************************** ;;

(bind-keys global-keymap "Super-x" 'cycle-terminal
                         "Super-C-x" '(run-shell-command terminal)
                         "Super-e" 'cycle-editor
                         "Super-C-e" '(run-shell-command editor)
                         "Super-z" 'cycle-browser
                         "Super-C-z" '(run-shell-command browser)
                         "Super-v" 'cycle-pdf-viewer
                         "Super-C-v" '(run-shell-command pdf)
                         "Super-d" 'cycle-proc-viewer
                         "Super-C-d" '(run-shell-command proc-viewer)
                         "Super-p" 'cycle-paint
                         "Super-C-p" '(run-shell-command paint)
                         "Super-v" 'cycle-video
                         "Super-S-s" '(run-shell-command "urxvtc -e periscope -l en $HOME/Downloads")
                         "Super-C-v" '(run-shell-command "xcmenu -p | xcmenu -ci")
                         "Super-m" 'cycle-mixer
                         "Super-C-m" '(run-shell-command mixer)
                         "Super-a" 'cycle-audio-player
                         "Super-C-a" '(run-shell-command audio)
                         "Super-s" 'cycle-fileman
                         "Super-C-s" '(run-shell-command fileman)
                         "Super-g" 'cycle-todo
                         "Super-grave" 'cycle-sawfish-config
                         "Super-C-grave" '(run-shell-command "sawfish-config")
                         )

;; *************************************** ;;
;; ************ General Keys ************* ;;
;; *************************************** ;;
;; *************************************** ;;

(bind-keys global-keymap "Super-1" '(activate-workspace 1)
                         "Super-2" '(activate-workspace 2)
                         "Super-3" '(activate-workspace 3)
                         "Super-4" '(activate-workspace 4)
                         "Super-space" '(run-shell-command "xboomx")
			 "Super-S-space" '(run-shell-command "dmenu -nb white -nf black -sb black -sf white")
                         "Super-C-space" '(run-shell-command "passmenu -nb white -nf black -sb black -sf white")
                         "Super-f" 'maximize-window-fullscreen-toggle
                         "Super-backslash" 'tile-tagged
                         "Super-c" 'move-cursor-center
                         "Super-C-c" '(run-shell-command "urxvtc -e glances -z")
                         "Super-Left" '(previous-workspace)
                         "Super-Right" '(next-workspace)
                         "Super-C-w" 'delete-window
                         "Super-C-Tab" 'cycle-class
                         "Super-W-r" 'restart
                         "Super-W-q" 'quit
                         )


(bind-keys root-window-keymap "button1-click1" '(popup-root-menu))

(bind-keys window-keymap "Super-F10" 'maximize-window-fullscreen-toggle
                         "Super-Button1-Click" 'toggle-tile-tag
                         "Super-Button3-Click" 'maximize-window-toggle
                         )

(bind-keys title-keymap "Button2-Off" '(run-shell-command "xte 'keydown Super_L' 'key Tab' 'keyup Super_L'")
                        )

;; *************************************** ;;
;; ********* Music Player Binds ********** ;;
;; *************************************** ;;
;; *************************************** ;;

(define mpd-keymap (make-keymap))
(bind-keys global-keymap "Super-S-m" mpd-keymap)

(bind-keys mpd-keymap "a" '(run-shell-command "mpc prev &>/dev/null")
                      "s" '(run-shell-command "mpc stop &>/dev/null")
                      "e" '(run-shell-command "mpc play &>/dev/null")
                      "d" '(run-shell-command "mpc next &>/dev/null")
                      "m" '(run-shell-command "mpc mute &>/dev/null")
                      "1" '(run-shell-command "amixer sset PCM 2-")
                      "2" '(run-shell-command "amixer sset PCM 2+"))
