(defun toggle-tile-tag (w)
  "Toggle whether a window is tileable (denoted by a [T] tag in window-title)"
  (if (equal (window-actual-group-id w) "Tiling")
             (progn
               (rename-window w (substring (window-name w) 4))
               (add-window-to-group w "Floating")
               )
    (rename-window w (concat "[T] " (window-name w)))
    (add-window-to-group w "Tiling")))

(define-command 'toggle-tile-tag toggle-tile-tag #:spec "%W")

(require 'smart-tile) ;; in 'lisp' directory

(defun tile-tagged ()
  (tile-windows)

  ;; Remove any tiled views no longer containing windows
  (delete-empty-workspaces)
  ;; Make a new workspace for each tile action containing only the
  ;; newly tiled windows
  (insert-workspace-after)
  (previous-workspace 1)

  (map-windows (lambda (x)
                 (if (equal (window-actual-group-id x) "Tiling")
                     (progn (copy-to-next-workspace x 1 nil)
                            (toggle-tile-tag x)
                            )))
               )
)

(define-command 'tile-tagged tile-tagged #:spec "%W")
