( require 'sawfish.wm.commands.x-cycle )

(eval-in
  `(let
       ((old-raise-window* raise-window*))
     (define (raise-window* win)
       (warp-cursor-to-window win)
       (old-raise-window* win)))
  'sawfish.wm.commands.x-cycle)
