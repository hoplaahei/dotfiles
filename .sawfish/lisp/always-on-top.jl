(defun toggle-window-always-on-top (w)
  "Toggle the depth of the window between 0 (default) and 1 (always on top)."
  (window-put w 'depth
	      (if (> (window-get w 'depth) 0) 0 1)))

(define-command 'toggle-window-always-on-top toggle-window-always-on-top #:spec "%W")
