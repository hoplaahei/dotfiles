;;; app.jl --- Application access commands

;; Copyright (C) 2009 Jeremy Hankins
;; $Date: 2009-03-29 09:43:33 -0500 (Sun, 29 Mar 2009) $
;; $Revision: 349 $

;; Author: Jeremy Hankins <nowan at nowan dot org>

;;; GPL blurb

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;;; Commentary

;; Create commands to acces applications: start them if needed,
;; otherwise cycle through its windows.

;;; Code:

(define-structure app

    (export app-selector
	    filter-byname
	    filter-byclass)

    (open rep
	  rep.regexp
	  rep.system
	  sawfish.wm.windows)

  (define (filter-byname re)
    "Return a predicate to match windows matching the given expression."
    (lambda (w)
      (and (window-in-cycle-p w)
	   (string-match re (window-name w)))))

  (define (filter-byclass class)
    "Return a predicate to match windows of the given class."
    (lambda (w)
      (and (window-in-cycle-p w)
	   (string-equal class (window-class w)))))

  (define (app-selector pred #!optional prog)
    "Create a selector for define-cycle-command to run prog if there are no
matching windows."
    (lambda ()
      (let ((windows (filter-windows pred)))
	(unless windows
	  (when prog
	    (if (functionp prog)
		(funcall prog)
	      (system (concat prog "&")))))
	windows)))

  )

;;; app.jl ends here


;;; Local Variables:
;;; mode:sawfish
;;; End:
