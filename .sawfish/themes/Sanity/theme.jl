;; Sanity/theme.jl

;;{{{ Copyrights, etc.

;; Copyright (C) 2001 Jason F. McBrayer

;; Sanity is an attempt at making a Sawfish theme that is
;; attractive, easy on the eyes, and customizable, while avoiding the
;; lure of gaudy graphics and crack-rock configuration options.  See
;; the file 'ui-guidelines' for details.

;; This is a preliminary release.  The next version of Sanity will be
;; significantly improved, with no pixmaps and possibly speedups. 

;; Portions of the code are taken from, based on, or informed by my
;; reading of the sawfish themes:
;;
;; "Aquarational" (C) 2000 Pedro Lopes <paol@teleweb.pt>
;; "Crux" (C) 2001 Eazel, Inc., by John Harper <jsh@eazel.com>
;; "twm" (C) 1999-2000 John Harper <john@dcs.warwick.ac.uk>
;; "HappyDesktop" (C) 2001 Andrew Chadwick <andrewc-20010402-hdt@piffle.org>
;; "Platinum+" (C) 2001 Jes�s Gonz�lez <jgonzlz@terra.es>

;; This theme is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public Licence as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; It is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public Licence for more details.
;;
;; You should have received a copy of the GNU General Public Licence
;; along with sawfish; see the file COPYING.  If not, write to
;; the Free Software Foundation, 675 Mass Ave, Cambridge (Our
;; Fair City), MA 02139, USA.
;;
;;}}}

;;{{{ Requires
(require 'gtkrc)
;(require 'sawfish.wm.util.gtkrc)
(require 'sawfish.wm.util.recolor-image)

;;}}}

;;{{{ Configuration options.

(defgroup Sanity "Sanity theme"
  :group appearance)

(defcustom sanity:button-layout 'Platinum+
  "Titlebar buttons layout"
  :group (appearance Sanity)
  :type symbol
  :user-level expert
  :options (Platinum+ Platinum System-7 NeXTStep twm)
  :after-set (lambda () (color-changed)))

(defcustom sanity:show-resize-handle 't
  "Show resize handle on corner of window."
  :group (appearance Sanity)
  :user-level expert
  :type boolean
  :after-set (lambda () (color-changed)))

; Decorate transients option left out for now, because it gets
; really eeville if someone manages to shade an undecorated
; transient window.  What happens to it?
;
;(defcustom sanity:decorate-transients 't
;  "Decorate transient windows."
;  :group (appearance Sanity)
;  :user-level expert
;  :type boolean
;  :after-set (lambda () (color-changed)))

(defcustom sanity:text-color nil
  "Color for active text, linedrawing."
  :group (appearance Sanity)
  :user-level intermediate
  :type (optional color)
  :after-set (lambda () (color-changed)))

(defcustom sanity:background-color nil
  "Color for text background, inactive titlebars."
  :group (appearance Sanity)
  :user-level intermediate
  :type (optional color)
  :after-set (lambda () (color-changed)))

(defcustom sanity:active-color nil
  "Color for active titlebars."
  :group (appearance Sanity)
  :user-level intermediate
  :type (optional color)
  :after-set (lambda () (color-changed)))

(defcustom sanity:inactive-text-color nil
  "Color for inactive text."
  :group (appearance Sanity)
  :user-level intermediate
  :type (optional color)
  :after-set (lambda () (color-changed)))

(defcustom sanity:tinyfont
  default-font
  "Small font to use for transient titlebars"
  :group (appearance Sanity)
  :user-level intermediate
  :type (optional font)
  :after-set (lambda () (color-changed)))

;;}}}

;;{{{ Misc. functions

(define initialize-gtkrc
  (let ((done nil))
    (lambda ()
      (unless done
        (require 'gtkrc)
        (gtkrc-reload-style)
        ;; recolour everything when the GTK theme changes
        (gtkrc-call-after-changed color-changed)
        (setq done t)
        ))))

(define (rebuild-all)
  (rebuild-frames-with-style 'Sanity))

(define (reframe-all)
  (reframe-windows-with-style 'Sanity))

(define (color-changed)
  (recolor-all)
  (rebuild-all)
  (reframe-all))

;;{{{ Vertical Text rendering by Simon Budig
(require 'x)

(define make-text-image
  (lambda (string
           #!optional
           (direction 1)
           (color (get-color "black"))
           (font default-font)
           )
    (let (drawable shapedrw gc image height width
                   (black (get-color "black"))
                   (white (get-color "white"))
                   )
      (setq width (text-width string font))
      (setq height (font-height font))
      
      ;; We just need to fill the main drawable with the desired color:
      ;; Text rendering is done with the Shape-Pixmap.
      (setq drawable (x-create-pixmap (cons width height)))
      (setq gc (x-create-gc drawable `((foreground . ,color))))
      (x-fill-rectangle drawable gc '(0 . 0) (cons width height))
      (x-destroy-gc gc)
      
      ;; Shape pixmap
      (setq shapedrw (x-create-pixmap (cons width height)))
      (setq gc (x-create-gc shapedrw `((foreground . ,black))))
      (x-fill-rectangle shapedrw gc '(0 . 0) (cons width height))
      (x-change-gc gc `((foreground . ,white)))
      ;; here are some font-properties hardcoded. 
      ;; font-ascent doesnt seem to be available in gaol...
      ;;(x-draw-string shapedrw gc '(0 . (font-ascent font)) string font)
      (x-draw-string shapedrw gc '(0 . 8) string font)
      (x-destroy-gc gc)
      
      (setq image (x-grab-image-from-drawable drawable shapedrw))
      (x-destroy-drawable drawable)
      (x-destroy-drawable shapedrw)
      (if (or (= direction 2) (= direction 4))
          (flip-image-diagonally image))
      (if (or (= direction 2) (= direction 3))
          (flip-image-horizontally image))
      (if (or (= direction 3) (= direction 4))
          (flip-image-vertically image))
      image
      )))
;;}}}

(define (window-name-vertical w)
  (if (fontp sanity:tinyfont)
      (make-text-image (window-name w) 4 (text-color) sanity:tinyfont) 
    (make-text-image (window-name w) 4 (text-color) )))

;; This doesn't work yet, or rather, there's no way to specify the two
;; different functions to the frame part :(
;(define (window-name-vertical-inactive w)
;  (if (fontp sanity:tinyfont)
;      (make-text-image (window-name w) 4 (inactive-text-color) sanity:tinyfont) 
;    (make-text-image (window-name w) 4 (inactive-text-color))))

;;}}}

;;{{{ Define our colours

(define (text-color)
  (if (colorp sanity:text-color) sanity:text-color
    (initialize-gtkrc)
    (if (colorp (car gtkrc-foreground))
        (car gtkrc-foreground)
      (get-color "black"))))

(define (background-color)
  (if (colorp sanity:background-color) sanity:background-color
    (initialize-gtkrc)
    (if (colorp (car gtkrc-background))
        (car gtkrc-background)
      (get-color "gray80"))))

(define (active-color)
  (if (colorp sanity:active-color) sanity:active-color
    (initialize-gtkrc)
    ; Try prelight background, unless it's the same as normal
    ; background.
    (if (and (colorp (nth 1 gtkrc-background))
             (not (eq (nth 1 gtkrc-background)
                      (car gtkrc-background))))
        (nth 1 gtkrc-background)
      ; Next try active background.
      (if (colorp (nth 3 gtkrc-background))
          (nth 3 gtkrc-background)
        (get-color "steelblue")))))

(define (inactive-text-color)
  (if (colorp sanity:inactive-text-color) sanity:inactive-text-color
    (initialize-gtkrc)
    ;How do we get the insensitive text gtkrc color?
    (if (colorp (car gtkrc-foreground))
        (car gtkrc-foreground)
      (get-color "gray40"))))

;;}}}

;;{{{ Images (basic)

;;{{{ normal windows
; Active titlebar
(define titlebar-active-image (make-image "titlebar.png"))
(set-image-border titlebar-active-image 2 2 0 0)
(define capsule-image (make-image "capsule.png"))
(set-image-border capsule-image 2 2 0 0)

; Inactive titlebar
(define titlebar-inactive-image (make-image "titlebar.png"))
(set-image-border titlebar-inactive-image 2 2 0 0)

; resize handle
(define resize-handle-image (make-image "resize-handle.png"))
(define null-resize-handle-image (make-sized-image 25 25 (get-color "black")))
(set-image-shape-color null-resize-handle-image (get-color "black"))
(define trans-resize-handle-image (make-image "resize-handle-transient.png"))
(define null-trans-resize-handle-image (make-sized-image 19 19
                                                         (get-color "black")))
(set-image-shape-color null-trans-resize-handle-image (get-color "black"))

;;}}}

;;{{{ transient windows
(define trans-title-active-image (make-image "title-transient.png"))
(set-image-border trans-title-active-image 0 0 2 2)
(define trans-title-inactive-image (make-image "title-transient.png"))
(set-image-border trans-title-inactive-image 0 0 2 2)
(define transient-capsule-image (make-image "capsule-transient.png"))
(set-image-border transient-capsule-image 0 0 2 2)

;;}}}

;;{{{ buttons

; transient-window close
(define trans-close-button-image (make-image "close-button-transient.png"))
(define trans-close-button-clicked-image
  (make-image "close-button-transient.png"))

; close button
(define close-button-image (make-image "close-button.png"))
(define close-button-clicked-image (make-image "close-button.png"))

; maximize button
(define maximize-button-image (make-image "maximize-button.png"))
(define maximize-button-clicked-image (make-image "maximize-button.png"))

; iconify button
(define iconify-button-image (make-image "iconify-button.png"))
(define iconify-button-clicked-image (make-image "iconify-button.png"))

; resize button
(define resize-button-image (make-image "resize-button.png"))
(define resize-button-clicked-image (make-image "resize-button.png"))

; shade button
(define shade-button-image (make-image "shade-button.png"))
(define shade-button-clicked-image (make-image "shade-button.png"))
;;}}}

;;}}}

;;{{{ Image recoloring

(define (recolor-all)
  (let ((active-recolorer
         (make-image-recolorer* (list (active-color) (text-color))
                                #:zero-channel red-channel
                                #:index-channels (list green-channel
                                                       blue-channel)
                                #:save-original t))
        (inactive-recolorer
         (make-image-recolorer* (list (background-color) (text-color))
                                #:zero-channel red-channel
                                #:index-channels (list green-channel
                                                       blue-channel)
                                #:save-original t))
        (inverse-recolorer
         (make-image-recolorer* (list (text-color) (background-color))
                                #:zero-channel red-channel
                                #:index-channels (list green-channel
                                                       blue-channel)
                                #:save-original t)))
    (mapc active-recolorer (list titlebar-active-image
                                 trans-title-active-image
                                 resize-handle-image
                                 trans-resize-handle-image
                                 ))
    (mapc inactive-recolorer (list capsule-image
                                   transient-capsule-image
                                   titlebar-inactive-image
                                   trans-title-inactive-image
                                   trans-close-button-image
                                   close-button-image
                                   iconify-button-image
                                   maximize-button-image
                                   resize-button-image
                                   shade-button-image
                                   ))
    (mapc inverse-recolorer (list trans-close-button-clicked-image
                                  close-button-clicked-image
                                  iconify-button-clicked-image
                                  maximize-button-clicked-image
                                  resize-button-clicked-image
                                  shade-button-clicked-image
                                  ))
    ))

;;}}}

;;{{{ Dimension calculation functions

(define capsule-width
  (lambda (w) ;FIXME
    ;(min (width of title-length + padding) and
    ;(window width - 2x max left vs right buttons-width - padding))
    (min (+ (text-width (window-name w)) 32)
         (- (car (window-dimensions w)) (* 2 (max (left-buttons-width)
                                                  (right-buttons-width)))))
    
         ))

(define capsule-offset
  (lambda (w)
    (/ (- (car (window-dimensions w)) (capsule-width w)) 2)))

(define transient-capsule-height
  (lambda (w) ;FIXME
    ; window height - a fixed amount of padding
    (min (- (cdr (window-dimensions w)) 48)
         (+ (text-width (window-name w) sanity:tinyfont) 16))
    ))

(define transient-capsule-offset
  (lambda (w)
    (/ (- (cdr (window-dimensions w)) (transient-capsule-height w)) 2)))

(define left-buttons-width
  (lambda () 
    ; each button is 16px; 6px padding between buttons; buttons are placed
    ; 2px in from edge of window.
    (+ 8 (* (length (left-buttons)) 16))
    ))

(define right-buttons-width
  (lambda ()
    (+ 8 (* (length (right-buttons)) 16))
    ))

;;}}}

;;{{{ Frame parts

;;{{{ Buttons
(define (trans-close-button)
  `((class . close-button)
    (background . (,(background-color)
                   ,trans-close-button-image
                   ,trans-close-button-image
                   ,trans-close-button-clicked-image))
    (top-edge . 2)
    (left-edge . -16)
    (width . 12)
    (height . 12)
    ))

; Please note that most buttons don't have left-right coordinates specified
; yet.  That will get done when building left-buttons and right-buttons.
(define (close-button)
  `((class . close-button)
    (background . (,(background-color)
                   ,close-button-image
                   ,close-button-image
                   ,close-button-clicked-image))
    (width . 16)
    (height . 16)
    (top-edge . -20)
    (removable . t)
    ))

(define (maximize-button)
  `((class . maximize-button)
    (background . (,(background-color)
                   ,maximize-button-image
                   ,maximize-button-image
                   ,maximize-button-clicked-image))
    (width . 16)
    (height . 16)
    (top-edge . -20)
    (removable . t)
    ))

(define (iconify-button)
  `((class . iconify-button)
    (background . (,(background-color)
                   ,iconify-button-image
                   ,iconify-button-image
                   ,iconify-button-clicked-image))
    (width . 16)
    (height . 16)
    (top-edge . -20)
    (removable . t)
    ))

(define (resize-button)
  ; resize-button isn't a standard class, but it should be.
  `((class . resize-button)
    (background . (,(background-color)
                   ,resize-button-image
                   ,resize-button-image
                   ,resize-button-clicked-image))
    (width . 16)
    (height . 16)
    (top-edge . -20)
    (removable . t)
    ))

(define (shade-button)
  `((class . shade-button)
    (background . (,(background-color)
                   ,shade-button-image
                   ,shade-button-image
                   ,shade-button-clicked-image))
    (width . 16)
    (height . 16)
    (top-edge . -20)
    (removable . t)
    ))


;;}}}

;;{{{ Other
(define (titlebar)
  `((class . title)
    (background . (,titlebar-inactive-image
                   ,titlebar-active-image
                   ,titlebar-active-image
                   ,titlebar-active-image))
    ;(foreground . (,(text-color)
    ;               ,(active-color)
    ;               ,(active-color)
    ;               ,(active-color)))
    ;(text . ,window-name)
    (x-justify . center)
    (y-justify . center)
    (top-edge . -24)
    (height . 24)
    (right-edge . -2)
    (left-edge . -2)
    ))
(define (capsule)
  `((class . title)
    (background . (,(background-color)
                   ,capsule-image
                   ,capsule-image
                   ,capsule-image))
    (foreground . (,(inactive-text-color)
                   ,(text-color)
                   ,(text-color)
                   ,(text-color)))
    (text . ,window-name)
    (x-justify . center)
    (y-justify . center)
    (top-edge . -21)
    (height . 18)
    (width . ,capsule-width)
    (right-edge . ,capsule-offset)
    (left-edge . ,capsule-offset)
    ))
(define (trans-titlebar)
  `((class . title)
    (background . (,trans-title-inactive-image
                   ,trans-title-active-image
                   ,trans-title-active-image
                   ,trans-title-active-image))
    (top-edge . -2)
    (bottom-edge . -2)
    (left-edge . -20)
    (width . 20)))

(define (trans-capsule)
  `((class . title)
    (background . (,(background-color)
                   ,transient-capsule-image
                   ,transient-capsule-image
                   ,transient-capsule-image))
    (foreground .  ,window-name-vertical)
    (x-justify . 2)
    (y-justify . center)
    (left-edge . -16)
    (width . 12)
    (top-edge . ,transient-capsule-offset)
    (height . ,transient-capsule-height)
    ))


(define (resize-handle)
  `((class . bottom-right-corner)
    (background . (,null-resize-handle-image
                   ,resize-handle-image
                   ,resize-handle-image
                   ,resize-handle-image))
    (bottom-edge . -6)
    (right-edge . -6)
    (below-client . t)
    ))

(define (trans-resize-handle)
  `((class . bottom-right-corner)
    (background . (,null-trans-resize-handle-image
                   ,trans-resize-handle-image
                   ,trans-resize-handle-image
                   ,trans-resize-handle-image))
    (bottom-edge . -6)
    (right-edge . -6)
    (below-client . t)
    ))

;;{{{ borders

; Note that borders are class title.  That's intentional.  You can
; drag windows around with them ala Macintosh.  You can't resize with
; them.
(define (border-top)
  `((class . title)
    (background . ,(text-color))
    (top-edge . -1)
    (left-edge . -1)
    (right-edge . -1)
    (height . 1)
    (width . ,(lambda (w) (car (window-dimensions w))))
    ))

(define (border-left)
  `((class . title)
    (background . ,(text-color))
    (top-edge . -1)
    (left-edge . -1)
    (bottom-edge . -1)
    (height . ,(lambda (w) (cdr (window-dimensions w))))
    (width . 1)
    ))

(define (border-right)
  `((class . title)
    (background . ,(text-color))
    (top-edge . -1)
    (bottom-edge . -1)
    (right-edge . -1)
    (height . ,(lambda (w) (cdr (window-dimensions w))))
    (width . 1)
    ))

(define (border-bottom)
  `((class . title)
    (background . ,(text-color))
    (bottom-edge . -1)
    (left-edge . -1)
    (right-edge . -1)
    (height . 1)
    (width . ,(lambda (w) (car (window-dimensions w))))
    ))

(define (borders)
  (list (border-top) (border-left) (border-right) (border-bottom)))

;;}}}

;;}}}

;;}}}

;;{{{ Button packing

(define (left-buttons)
  (cond
   ((eq sanity:button-layout 'Platinum+)
    (list (cons '(left-edge . 2) (close-button))))
   ((eq sanity:button-layout 'Platinum)
    (list (cons '(left-edge . 2) (close-button))))
   ((eq sanity:button-layout 'System-7)
    (list (cons '(left-edge . 2) (close-button))))
   ((eq sanity:button-layout 'NeXTStep)
    (list (cons '(left-edge . 2) (iconify-button))))
   ((eq sanity:button-layout 'twm)
    (list (cons '(left-edge . 2) (iconify-button))))
   (t (list (cons '(left-edge . 2) (close-button))))
   ))

(define (right-buttons)
  (cond
   ((eq sanity:button-layout 'Platinum+)
    `(,(cons '(right-edge . 2) (iconify-button))
      ,(cons '(right-edge . 20) (maximize-button)))
      )
   ((eq sanity:button-layout 'Platinum)
    `(,(cons '(right-edge . 2) (shade-button))
      ,(cons '(right-edge . 20) (maximize-button)))
    )
   ((eq sanity:button-layout 'System-7)
    (list (cons '(right-edge . 2) (maximize-button))))
   ((eq sanity:button-layout 'NeXTStep)
    `(,(cons '(right-edge . 2) (close-button))))
   ((eq sanity:button-layout 'twm)
    `(,(cons '(right-edge . 2) (resize-button))))
   (t nil)
   ))

;;}}}

;;{{{ Frames

(define (normal-frame)
  `(,@(borders) ,(titlebar) ,(capsule)
              ,(if sanity:show-resize-handle (resize-handle))
              ,@(left-buttons) ,@(right-buttons)
              ))
(define (shaped-frame) (borders))
(define (shaded-frame)
  `(,(titlebar) ,(capsule)
              ,@(left-buttons) ,@(right-buttons)
              ))

(define (transient-frame)
  `(,@(borders) ,(trans-titlebar) ,(trans-close-button)
              ,(trans-capsule)
              ,(if sanity:show-resize-handle (trans-resize-handle))
              ))
(define (shaped-transient-frame) (borders))
(define (shaded-transient-frame)
  (list (trans-titlebar) (trans-close-button) (trans-capsule)
        ))

;;}}}

;;{{{ Initialization

; set up initial colors
(recolor-all)

; recalibrate frames when window-name changes
(call-after-property-changed 'WM_NAME rebuild-frame)

; resize-button frame part
(def-frame-class resize-button '((cursor . left_ptr))
       (bind-keys resize-button-keymap
         "Button1-Move" 'resize-window-interactively))

; register the theme
(add-frame-style 'Sanity
                 (lambda (w type)
                   (case type
                     ((default) (normal-frame))
                     ((shaped) (shaped-frame))
                     ((shaded) (shaded-frame))
                     ((transient) (transient-frame))
                     ((shaped-transient) (shaped-transient-frame))
                     ((shaded-transient) (shaded-transient-frame))
                     )))

;;}}}